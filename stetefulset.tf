resource "kubernetes_stateful_set" "rabbitmq" {

  metadata {
    name      = local.name
    namespace = var.namespace
  }

  spec {
#    pod_management_policy = var.pod_management_policy
    replicas              = var.replicas

    selector {
      match_labels = {
        app = local.name
      }
    }

    service_name = kubernetes_service.rabbitmq.metadata.0.name
#     service_name = "rabbitmq"

    template {

      metadata {
        labels = {
            app = local.name
           }
      }

      spec {
        service_account_name            = kubernetes_service_account.rabbitmq.metadata.0.name
#         service_account_name = "rabbitmq"
#        automount_service_account_token = true



        init_container {
          name  = "config"
          image = "busybox"
          command = [
            "/bin/sh",
            "-c",
            "cp /tmp/config/rabbitmq.conf /config/rabbitmq.conf && ls -l /config/ && cp /tmp/config/enabled_plugins /etc/rabbitmq/enabled_plugins"
          ]

          volume_mount {
            name       = "config"
            mount_path = "/tmp/config/"
            read_only  = false
          }
          volume_mount {
            name       = "config-file"
            mount_path = "/config/"
          }
          volume_mount {
            name       = "plugins-file"
            mount_path = "/etc/rabbitmq/"
          }
        }

        container {
          name              = "rabbitmq"
          image             = "${var.image_name}:${var.image_tag}"
#          image_pull_policy = var.image_pull_policy

#          security_context {
#            run_as_user  = var.user_id
#            run_as_group = var.group_id
#          }

          dynamic "port" {
            for_each = local.enabled_ports

            content {
              name           = port.value["name"]
              protocol       = port.value["protocol"]
              container_port = port.value["port"]
            }

          }

          env {
            name = "RABBIT_POD_NAME"
            value_from {
              field_ref {
                api_version = "v1"
                field_path  = "metadata.name"
              }
            }
          }

          env {
            name = "RABBIT_POD_NAMESPACE"
            value_from {
              field_ref {
                api_version = "v1"
                field_path  = "metadata.namespace"
              }
            }
          }

#          env {
#            name = "K8S_ADDRESS_TYPE"
#            value = "hostname"
#                }
          env {
            name = "RABBITMQ_NODENAME"
            value = "rabbit@$(RABBIT_POD_NAME).rabbitmq.$(RABBIT_POD_NAMESPACE).svc.cluster.local"
                }
          env {
            name = "K8S_HOSTNAME_SUFFIX"
            value = ".rabbitmq.$(RABBIT_POD_NAMESPACE).svc.cluster.local"
                }

          env {
            name = "RABBITMQ_USE_LONGNAME"
            value = "true"
                }
#          env {
#            name = "K8S_HOST"
#            value = ""
#                }
#          env {
#            name = "K8S_PORT"
#            value = "6443"
#                }
          env {
            name = "RABBITMQ_CONFIG_FILE"
            value = "/config/rabbitmq"
                }


          env {
            name = "RABBITMQ_ERLANG_COOKIE"
            value_from {
              secret_key_ref {
                name = kubernetes_secret.rabbitmq.metadata.0.name
                key  = "erlang_cookie"
              }
            }
          }



#          env {
#            name = "RABBITMQ_DEFAULT_USER"
#            value_from {
#              secret_key_ref {
#                name = kubernetes_secret.rabbitmq.metadata.0.name
#                key  = "default_user"
#              }
#            }
#          }

#          env {
#            name = "RABBITMQ_DEFAULT_PASS"
#            value_from {
#              secret_key_ref {
#                name = kubernetes_secret.rabbitmq.metadata.0.name
#                key  = "default_pass"
#              }
#            }
#          }

          volume_mount {
            name       = "data"
            mount_path = "/var/lib/rabbitmq"
            read_only  = false
          }

          volume_mount {
            name       = "config-file"
            mount_path = "/config/"
          }
          volume_mount {
            name       = "plugins-file"
            mount_path = "/etc/rabbitmq/"
          }


          liveness_probe {
            period_seconds        = 30
            initial_delay_seconds = 5
            exec {
              command = [
                "rabbitmq-diagnostics", "-q", "ping"
              ]
            }
          }
        }

#        dns_config { 
#          searches = ["${kubernetes_service.rabbitmq.metadata.0.name}.${var.namespace}.svc.${var.cluster_domain}"]
#        }

        volume {
          name = "config-file"
          empty_dir {}
        }

        volume {
          name = "plugins-file"
          empty_dir {}
        }

        volume {
          name = "config"
          config_map {
            name = "rabbitmq-config"
            default_mode = "0755"
          }
        }
      }
    }

    update_strategy {
      type = "RollingUpdate"

      rolling_update {
        partition = 1
      }
    }

    volume_claim_template {
      metadata {
        name = "data"
      }

      spec {
        access_modes = ["ReadWriteOnce"]

        resources {
          requests = {
             storage = "${var.persistence_size}"
          }

        }

       storage_class_name = "${var.persistence_storage_class}"
      }

    }

  }

}
