Terraform K8s RabbitMQ Module
=============================

Terraform module to create following K8S resources:
- StatefulSet
- Service
- Secret
- ConfigMap
- Ingress (optionally)

# Contents
- [Required Input Variables](#variables)
- [Usage](#usage)
- [Licence](#licence)
- [Outputs](#outputs)
- [Author Information](#author)

## <a name="variables"></a> Required Input Variables
Module does not require any input variables. See [full list](variables.tf) of supported variables

## <a name="usage"></a> Usage

To provision 3-nodes cluster in default namespace use:
```hcl-terraform
module "rabbitmq" {
  source = "./modules/rabbitmq_k8s"
}
```

To enable rabbitmq_management plugin and expose it via cluster ingress controller use:
```hcl-terraform
module "rabbitmq" {
  source = "./modules/rabbitmq_k8s"
  additional_plugins = [
    "rabbitmq_management","rabbitmq_prometheus"
  ]
   ingress_annotations = {
    "nginx.ingress.kubernetes.io/ssl-redirect" = "true"
    "nginx.ingress.kubernetes.io/from-to-www-redirect" = "true"
    "kubernetes.io/ingress.class" = "nginx"

  }

  ingress_hosts = [
    {
      host = "rabbitmq.${local.domain_name}"
      path = "/"
    }
  ]
}
```

## <a name="outputs"></a> Outputs
Full list of module's outputs and descriptions can be found in [outputs.tf](outputs.tf)

## <a name="license"></a> License
The module is being distributed under [MIT Licence](LICENCE.txt). Please make sure you have read, understood and agreed
to its terms and conditions

## <a name="author"></a> Author Information
Vladimir Trisheckin <tva2002@gmail.com><br/>Ukraine